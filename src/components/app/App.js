import { useState } from 'react';
import { PhotoAlbum } from 'react-photo-album';
import Zoom from 'yet-another-react-lightbox/plugins/zoom';
import Lightbox from 'yet-another-react-lightbox';

import { photos } from '../../constants/images';
import 'yet-another-react-lightbox/styles.css';

const renderPhoto = ({ imageProps: { alt, style, ...restImageProps } }) => (
  <div className="image-container">
    <img
      alt={alt}
      style={{ ...style, width: '100%', padding: 0 }}
      {...restImageProps}
    />
  </div>
);

export const App = () => {
  const [index, setIndex] = useState(-1);

  return (
    <div>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          position: 'fixed',
          top: '0',
          right: '0',
          padding: '8px',
          marginRight: '48px',
          alignItems: 'flex-end',
          zIndex: '10',
        }}
      >
        <a
          href="https://www.instagram.com/katxinh/"
          rel="noreferrer"
          target="_blank"
        >
          INSTAGRAM
        </a>
        <a href="mailto:nkhanh.vt@gmail.com" rel="noreferrer" target="_blank">
          EMAIL
        </a>
      </div>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          margin: '6rem 0rem 6rem',
          cursor: 'pointer',
        }}
      >
        <h1
          style={{
            marginBottom: '8px',
          }}
        >
          KHANH VU
        </h1>
        <h3>MODEL</h3>
      </div>
      <div
        style={{
          padding: '0rem 1.5rem 1.5rem',
          textAlign: 'center',
        }}
      >
        <span>
          Height - 155cm, Waist - 86cm, Bust - 110cm, Hips - 110cm, Shoe - 37cm,
          Eyes - Brown
        </span>
        <PhotoAlbum
          photos={photos}
          layout="masonry"
          spacing={32}
          columns={(containerWidth) => {
            if (containerWidth < 400) return 2;
            if (containerWidth < 800) return 3;
            return 4;
          }}
          onClick={({ index }) => setIndex(index)}
          renderPhoto={renderPhoto}
        />
        <Lightbox
          slides={photos}
          open={index >= 0}
          index={index}
          close={() => setIndex(-1)}
          plugins={[Zoom]}
        />
      </div>
    </div>
  );
};
